/* Bài 1: Viết 3 số nguyên theo thứ tự tăng dần
- Đầu vào:
Khai báo 3 biến số nguyên 1, 2 3
- Xử lý:
Cho từng trường hợp xuất ra thứ tự tăng dần cho từng trường hợp
- Đầu ra:
Số sắp xếp theo thứ tự tăng dần.
 */
function sapXep() {
  var so1 = document.getElementById("soNguyen1").value;
  var so2 = document.getElementById("soNguyen2").value;
  var so3 = document.getElementById("soNguyen3").value;
  if (so1 < so2 && so2 < so3) {
    thuTu = so1 + so2 + so3;
  } else if (so1 < so2 && so2 > so3 && so3 < 1) {
    thuTu = so1 + so3 + so2;
  } else if (so2 < so1 && so1 < so3) {
    thuTu = so2 + so1 + so3;
  } else if (so2 < so1 && so1 > so3 && so2 < 1) {
    thuTu = so2 + so3 + so1;
  } else if (so3 < so1 && so1 < so2) {
    thuTu = so3 + so1 + so2;
  } else if (so3 < so1 && so1 > so2) {
    thuTu = so3 + so2 + so1;
  } else {
    return;
  }
  document.getElementById("result").innerHTML = `<p">Thứ tự sắp xếp:</p>
    <h2>${thuTu}</h2>`;
}
/*Bài 2: Chào hỏi các thành viên trong gia đình:
+ Đầu vào:
Gán id = tv (thành viên đăng nhập)
+ Xử lý:
B = Bố; M = Mẹ; A= Anh trai; E= Em gái
If từng trường hợp, đúng với kí tự nào thì chào người đó (tv =B || tv = M || tv = A || tv = E)
+ Đầu ra:
Xuất ra màn hình Xin chào thành viên ....*/
function xinChao() {
  var tv = document.getElementById("thanhvien").value;
  var B =document.getElementById("bo").value;
  var M =document.getElementById("me").value;
  var A =document.getElementById("anh").value;
  var E =document.getElementById("em").value;
  if (tv == B) {
    document.getElementById("result2").innerHTML = `<p">Xin Chào Bố</p>`;
  } else if (tv == M) {
    document.getElementById("result2").innerHTML = `<p">Xin Chào Mẹ</p>`;
  } else if (tv == A) {
    document.getElementById("result2").innerHTML = `<p">Xin Chào Anh Trai</p>`;
  } else if (tv == E) {
    document.getElementById("result2").innerHTML = `<p">Xin Chào Em Gái</p>`;
  } else {document.getElementById("result2").innerHTML = `<p">Xin Chào Người Lạ</p>`;}
}
/*Bài 3: Đếm chẵn lẻ của 3 số nguyên
+ Đầu vào:
Gán 3 số cho s1, s2, s3
+ Xử lý: do có 3 trường hợp nên chia làm 3 trường hợp
3 số đều dương
2 số dương 
Trường hợp còn lại
+ Đầu ra: Đếm bao nhiêu số chẵn, số lẻ
 */
function demSo() {
  var s1 = document.getElementById("so1").value * 1;
  var s2 = document.getElementById("so2").value * 1;
  var s3 = document.getElementById("so3").value * 1;
  if (s1 % 2 == 0 && s2 % 2 == 0 && s3 % 2 == 0) {
    document.getElementById(
      "result3"
    ).innerHTML = `<p>Số Chẵn Có: 3 số</p><p>Số Lẻ Có: 0 số</p>`;
  } else if (s1 % 2 == 0 && s2 % 2 == 0 && s3 % 2 == 1) {
    document.getElementById(
      "result3"
    ).innerHTML = `<p>Số Chẵn Có: 2 số</p><p>Số Lẻ Có: 1 số</p>`;
  } else if (s1 % 2 == 1 && s2 % 2 == 0 && s3 % 2 == 0) {
    document.getElementById(
      "result3"
    ).innerHTML = `<p>Số Chẵn Có: 2 số</p><p>Số Lẻ Có: 1 số</p>`;
  } else if (s1 % 2 == 0 && s2 % 2 == 1 && s3 % 2 == 0) {
    document.getElementById(
      "result3"
    ).innerHTML = `<p>Số Chẵn Có: 2 số</p><p>Số Lẻ Có: 1 số</p>`;
  } else if (s1 % 2 == 0 && s2 % 2 == 1 && s3 % 2 == 1) {
    document.getElementById(
      "result3"
    ).innerHTML = `<p>Số Chẵn Có: 1 số</p><p>Số Lẻ Có: 2 số</p>`;
  } else {
    document.getElementById(
      "result3"
    ).innerHTML = `<p>Số Chẵn Có: 1 số</p><p>Số Lẻ Có: 2 số</p>`;
  }
}
/*Bài 4:
+ Đầu vào: 
Gán 3 cạnh tương ứng: a, b, c
+ Xử lý:
Kiểm tra 3 cạnh tam giác: 
a + b <= c;  a + c <= b; b + c <= a
Rồi đưa công thức tính của từng tam giác: tam giác đều nếu 3 cạnh bằng nhau; tam giác cân nếu 2 cạnh bằng nhau; tam giác vuông nếu: c^2 = a^2 + b^2
 */

function canhTamGiac() {
  var a = document.getElementById("canh1").value * 1;
  var b = document.getElementById("canh2").value * 1;
  var c = document.getElementById("canh3").value * 1;
  if (a + b <= c || a + c <= b || b + c <= a) {
    document.getElementById(
    "result4"
    ).innerHTML = `<p>Tam giác không hợp lệ, vui lòng nhập lại</p>`;
  } else if (
    a * a + b * b == c * c ||
    a * c + c * c == b * b ||
    b * b + c * c == a * c
  ) {
    document.getElementById("result4").innerHTML = `<p>Tam giác vuông</p>`;
  } else if (a == b && b == c) {
    document.getElementById("result4").innerHTML = `<p>Tam giác đều</p>`;
  } else if (a == b || a == c || b == c) {
    document.getElementById("result4").innerHTML = `<p>Tam giác cân</p>`;
  } else {
    document.getElementById("result4").innerHTML = `<p>Tam giác thường</p>`;
  }
}

/**
 * Bài 5: Nhập vào tháng năm ngày
 * Đầu vào: lấy value của user nhập vào
 * Xử lý: tính ra ngày tháng năm tiếp theo.
 *  + kiểm tra năm nhuận 
 *  + trường hợp tháng 2 && tháng 12 và tháng 1 ( trường hợp đặc biệt)
* Đầu ra: xuất ra hôm qa và ngày mai
*/
document.getElementById('ngayMai').onclick = function() {
    var date = document.getElementById('date').value*1;
    var month = document.getElementById('month').value*1;
    var year = document.getElementById('year').value*1;
    var nhuan;
    var soNgay = 0;
    // Kiểm tra năm nhuận
    if(year%4==0 && year%100!=0 || year%400==0) {
        nhuan = true;
    } else {
        nhuan = false;
    }
    switch(month) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12: {
            soNgay = 31;
        } break;
        case 2: {
            if(nhuan === true) {
                soNgay = 29;
            } else {
                soNgay = 28;
            }
        } break;
         case 4: case 6: case 9: case 11: {
            soNgay = 30;
        } break;
        default: {
            alert('only 12 month')
        }
    }
    var dTT;
    var mTT;
    var yTT;
    if(date === soNgay && year >0) {
        if (month != 12) {
            dTT =1;
            mTT = month + 1;
            yTT = year;
        } else {
            dTT =1;
            mTT = 1;
            yTT = year + 1;
        }
      
    } else if (date < soNgay && year >0 ) {
        dTT = date + 1;
        mTT = month;
        yTT = year;
    }  else {
        alert('Bạn đã nhập sai ngày tháng năm')
    }
    document.getElementById('result__ngayMai').innerHTML = `Ngày mai: ${dTT} / ${mTT} / ${yTT} <hr />`
}
document.getElementById('homQua').onclick = function() {
    var date = document.getElementById('date').value*1;
    var month = document.getElementById('month').value*1;
    var year = document.getElementById('year').value*1;
    var nhuan;
    var soNgay = 0;
    if(year%4==0 && year%100!=0 || year%400==0) {
        nhuan = true;
    } else {
        nhuan = false;
    }
    switch(month) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12: {
            soNgay = 31;
        } break;
        case 2: {
            if(nhuan === true) {
                soNgay = 29;
            } else {
                soNgay = 28;
            }
        } break;
         case 4: case 6: case 9: case 11: {
            soNgay = 30;
        } break;
        default: {
            alert('only 12 month')
        }
    }
    var dTD;
    var mTD;
    var yTD;
    if(date === 1 && year >0) {
        if (month === 3 && nhuan === true) {
            dTD =29;
            mTD = month - 1;
            yTD = year;
        } else if (month === 3 && nhuan === false){
            dTD = 28;
            mTD = month -1;
            yTD = year;
        } else if (month === 1) {
            dTD = 31;
            mTD = 12;
            yTD = year - 1;
        } else if (soNgay === 31) {
            dTD = 31;
            mTD = month - 1;
            yTD = year;
        } else {
            dTD = 30;
            mTD =month -1;
            yTD = year
        }
    } else if (date <= soNgay && year >0) {
        dTD = date - 1;
        mTD = month;
        yTD = year;
    } 
    else {
        alert('Bạn đã nhập sai ngày tháng năm')
    }
    document.getElementById('result__homQua').innerHTML = `<br /> <br /> <br /> Hôm Qua: ${dTD} / ${mTD} / ${yTD} `
}

/**Bài 6
 * Viết chương trình nhập vào tháng, năm. Cho biết tháng đó có bao nhiêu ngày. (bao gồm tháng
của năm nhuận).
+ Đầu vào: lấy giá trị người dùng nhập vào
+ Xử lí: year && month > 0 && month < 13   
        Test value year === namNham thì tháng 2 có 29 date
        value year !== namNhan thi thang 2 co 28 date
        month 1,3,5,7,8,10,12 => 31 date
        month 2,4,6,9,11 ==> 30 date.
+ Đầu ra: month user nhập có bao nhiêu ngày 
 */
document.getElementById('click__bai2').onclick = function() {
    var thang = document.getElementById('thang').value*1;
    var nam = document.getElementById('nam').value*1;
    var test;
    var ngayTrongThang;
    if(nam%4==0 && nam%100!=0 || nam%400==0) {
        test = true;
    } else {
        test = false;
    }
    switch(thang) {
        case 1: case 3:case 5: case 7:case 8: case 10:case 12: {
            ngayTrongThang = 31;
        } break;
        case 2: {
            if(test === true) {
                ngayTrongThang = 29;
            } else {
                ngayTrongThang = 28;
            }
        } break;
        case 2: case 4:case 6: case 9:case 11: {
            ngayTrongThang = 30;
        } break;
        default: {
            alert('month = 1 --> 12');
        }
    }
    document.getElementById('result__bai2').innerHTML = `Tháng này có ${ngayTrongThang} ngày`
}

/**
 * Bài 7: Viết chương trình nhập vào số nguyên có 3 chữ số. In ra cách đọc nó. 
 * Đầu vào: lấy value user
 * Xử lí: lấy giá trị từng hàng: trăm chục đơn vị
 * ==> cách đọc rồi cộng lại với nhau
 * Đầu ra: nhập vào cách đọc của user
 */

document.getElementById('click__bai3').addEventListener('click', function() {
    var read = document.getElementById('read').value*1;
    var test;
    if(read>= 100 && read<=999) {
test === true;
    } else {
        alert('Bạn hãy nhập số có 3 chứ số đảm bảo điều kiện');
    };
    var readTram = Math.floor(read / 100);
    var readChuc = Math.floor((read / 10)  % 10);
    var readDV = Math.floor(read % 10) 
    var hangTram= '';
    switch(readTram) {
        case 1: {
            hangTram = "Một trăm";
        } break;
        case 2:{
            hangTram = "Hai trăm";
        } break;
        case 3: {
            hangTram = "Ba trăm";
        } break;
        case 4: {
            hangTram = "Bốn trăm";
        } break;
        case 5:{
            hangTram = "Năm trăm";
        } break;
        case 6:{
            hangTram = "Sáu trăm";
        } break;
        case 7: {
            hangTram = "Bảy trăm";
        } break;
        case 8: {
            hangTram = "Tám trăm";
        } break;
        case 9: {
            hangTram = "Chín trăm";
        } break;
    }
    console.log(hangTram);
    var read_Chuc = '' ;
    if(readChuc % 10 == 0 && readDV != 0) {
        read_Chuc = 'lẻ';
    } else {
        switch(readChuc) {
            case 1: {
                read_Chuc = "mười";
            } break;
            case 2:{
                read_Chuc = "hai mươi ";
            } break;
            case 3: {
                read_Chuc = "Ba mươi ";
            } break;
            case 4: {
                read_Chuc = "Bốn mươi ";
            } break;
            case 5:{
                read_Chuc = "Năm mươi ";
            } break;
            case 6:{
                read_Chuc = "Sáu mươi ";
            } break;
            case 7: {
                read_Chuc = "Bảy mươi ";
            } break;
            case 8: {
                read_Chuc = "Tám mươi ";
            } break;
            case 9: {
                read_Chuc = "Chín mươi ";
            } break;
        }
    }
    console.log(read_Chuc);
    var read_DV = '' ;
    switch(readDV) {
        case 1: {
            read_DV = "một";
        } break;
        case 2:{
            read_DV = "hai";
        } break;
        case 3: {
            read_DV = "Ba";
        } break;
        case 4: {
            read_DV = "Bốn";
        } break;
        case 5:{
            read_DV = "Năm";
        } break;
        case 6:{
            read_DV = "Sáu";
        } break;
        case 7: {
            read_DV = "Bảy";
        } break;
        case 8: {
            read_DV = "Tám";
        } break;
        case 9: {
            read_DV = "Chín";
        } break;
    }
    console.log(read_DV);
    document.getElementById('result__bai3').innerHTML = `${hangTram} ${read_Chuc} ${read_DV}`
}
)

/**Bài 8:
 * Đầu vào: lấy giá trị tọa độ của 3 sv và trường học nhập vào
 * Xử lý: dùng công thức tính ra độ dài của từng sinh viên
 *          so sánh độ dài của từng sinh viên => sinh viên xa trường Nhất
 * Đầu ra: Xuất ra name của Sv xa trường nhất
 */

document.getElementById('click__bai4').addEventListener('click', function() {
    var name1 = document.getElementById('sinhVien1').value;
    var name2 = document.getElementById('sinhVien2').value;
    var name3 = document.getElementById('sinhVien3').value;
    var x1 = document.getElementById('x1').value*1;
    var x2 = document.getElementById('x2').value*1;
    var x3 = document.getElementById('x3').value*1;
    var x4 = document.getElementById('x4').value*1;
    var y1 = document.getElementById('y1').value*1;
    var y2 = document.getElementById('y2').value*1;
    var y3 = document.getElementById('y3').value*1;
    var y4 = document.getElementById('y4').value*1;
    var test_4;
    if (x1 >0&& x2 >0  && x3 >0 && x4 >0 && y1 >0 && y2 >0 && y3 > 0 && y4 > 0) {
        test = true;
    } else {
        alert('Tọa độ bạn nhập không đúng');
    }
    var distance_1;
    distance_1 = Math.sqrt((x4-x1)*(x4-x1)+(y4-y1)*(y4-y1));
    var distance_2;
    distance_2 = Math.sqrt((x4-x2)*(x4-x2)+(y4-y2)*(y4-y2));
    var distance_3;
    distance_3 = Math.sqrt((x4-x3)*(x4-x3)+(y4-y3)*(y4-y3));
    if (distance_1 === distance_2 === distance_3) {
        document.getElementById('result__bai4').innerHTML = `Khoảng đường của 3 sinh viên ngang nhau`
    } else {
        if (distance_1 > distance_2 && distance_1 >distance_3 ) {
            document.getElementById('result__bai4').innerHTML = `Sinh viên ${name1} xa nhất`
        } else if (distance_2 > distance_1 && distance_2 >distance_3) {
            document.getElementById('result__bai4').innerHTML = `Sinh viên ${name2} xa nhất`
        } else {
            document.getElementById('result__bai4').innerHTML = `Sinh viên ${name3} xa nhất`
        }
    }
})